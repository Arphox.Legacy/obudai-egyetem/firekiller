﻿using System.Windows;

namespace FireKiller_GLBXQU
{
    class Jatekos : TablaObjektum
    {
        public Jatekos(Point kezdőPozíció) //a játékos létrehozásakor meg kell adni a kezdő pozíciót
        {
            this.x = (int)kezdőPozíció.X;
            this.y = (int)kezdőPozíció.Y;
        }

        /// <summary>
        /// Megadja, hogy a játékos éppen életben van-e
        /// </summary>
        bool életbenVan = true;
        /// <summary>
        /// Megadja, hogy a játékos éppen életben van-e
        /// </summary>
        public bool ÉletbenVan
        {
            get { return életbenVan; }
            set { életbenVan = value; }
        }
    }
}