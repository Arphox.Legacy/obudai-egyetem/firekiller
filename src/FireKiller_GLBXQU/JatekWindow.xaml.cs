﻿using System;
using System.Windows;               //Window
using System.Windows.Controls;      //Grid
using System.Windows.Input;         //KeyEventArgs
using System.Windows.Media;         //MediaPlayer
using System.Windows.Media.Imaging; //BitmapImage
using System.IO;                    //File
using System.Windows.Threading;     //DispatcherTimer
using System.Diagnostics;           //StopWatch
using System.Collections.Generic;   //List<T>

namespace FireKiller_GLBXQU
{
    public enum Irány { Balra, Jobbra, Fel, Le }

    public partial class JatekWindow : Window
    {
        static Grid gridsave;                          //grid elmentéséhez használt Grid

        Palya pálya;                                   //Pálya objektum a pálya eltárolásához
        Jatekos játékos;                               //Játékos objektum a játékos eltárolásához
        internal Jatekos Játékos                       //Getter a játékos objektumhoz
        {
            get { return játékos; }
        }
        DispatcherTimer timer = new DispatcherTimer(); //Időzítő a tüzek mozgatásához
        Stopwatch stopper = new Stopwatch();           //Stopper [StopWatch] a pályaidő méréséhez

        //A gyakran hívott metódusok változói ki vannak emelve a kód optimalitása miatt
        //TüzeketMozgat()-hoz tartozók:
        /// <summary>
        /// Az Irány enum értékeit tároló tömb, a random irányok generálásához kell
        /// </summary>
        Array irányÉrtékek = Enum.GetValues(typeof(Irány));
        Random r = new Random();                                //véletlenszám generáló
        //PályaFrissítés()-hez és KépeketListábaBerak()-hoz tartozók:
        Grid grid = new Grid();
        string útvonalEleje = String.Format("textures/{0}/", Settings.KiválasztottTextúraCsomag); //az elérési útvonal elejét beállítja a kiválaszott textúra csomag beállítás alapján
        FileInfo fileinfo_path, fileinfo_stone, fileinfo_player, fileinfo_fire; //A betöltendő fájlok FileInfo-i
        Image img; //Image objektum, ezeket rakjuk majd bele a képek listájába

        //képeket tároló listák, ezekből történik a gridre való kiírás
        List<Image> utak = new List<Image>();
        List<Image> kövek = new List<Image>();
        List<Image> tüzek = new List<Image>();
        Image játékosKép;
        int utak_index = 0, kövek_index = 0, tüzek_index = 0;

        public JatekWindow(Palya pálya) //beállítja a pálya, játékos és FileInfo objektumok értékét
        {
            InitializeComponent();
            this.pálya = pálya;
            játékos = new Jatekos(pálya.JátékosKezdetiPozíció);
            fileinfo_path   = new FileInfo(útvonalEleje + "path.png");
            fileinfo_fire   = new FileInfo(útvonalEleje + "fire.png");
            fileinfo_player = new FileInfo(útvonalEleje + "player.png");
            fileinfo_stone  = new FileInfo(útvonalEleje + "stone.png");
        }

        private void KépeketListábaBerak()
        {
            for (int sor = 0; sor < pálya.Tábla.GetLength(0); sor++)
            {
                for (int oszlop = 0; oszlop < pálya.Tábla.GetLength(1); oszlop++)
                {
                    img = new Image();
                    if (pálya.Tábla[sor, oszlop] == 0) //út
                    {
                        if (Settings.BackupTextúraCsomagHasználata)
                            img.Source = new BitmapImage(new Uri(@"/DefaultTextures/path.png", UriKind.Relative));
                        else
                            img.Source = new BitmapImage(new Uri(fileinfo_path.FullName, UriKind.Absolute));
                        utak.Add(img);
                    }
                    if (pálya.Tábla[sor, oszlop] == 1) //kő
                    {
                        if (Settings.BackupTextúraCsomagHasználata)
                            img.Source = new BitmapImage(new Uri(@"/DefaultTextures/stone.png", UriKind.Relative));
                        else
                            img.Source = new BitmapImage(new Uri(fileinfo_stone.FullName, UriKind.Absolute));
                        kövek.Add(img);
                    }
                    if (pálya.Tábla[sor, oszlop] == 2) //ember
                    {
                        if (Settings.BackupTextúraCsomagHasználata)
                            img.Source = new BitmapImage(new Uri(@"/DefaultTextures/player.png", UriKind.Relative));
                        else
                            img.Source = new BitmapImage(new Uri(fileinfo_player.FullName, UriKind.Absolute));
                        játékosKép = img;
                    }
                    if (pálya.Tábla[sor, oszlop] == 3) //tűz
                    {
                        if (Settings.BackupTextúraCsomagHasználata)
                            img.Source = new BitmapImage(new Uri(@"/DefaultTextures/fire.png", UriKind.Relative));
                        else
                            img.Source = new BitmapImage(new Uri(fileinfo_fire.FullName, UriKind.Absolute));
                        tüzek.Add(img);
                    }

                    //nem a bal felső, hanem a bal alsó sarok lesz a (0,0) koordinátájú pont
                    Grid.SetRow(img, pálya.Tábla.GetLength(0) - 1 - sor);
                    Grid.SetColumn(img, oszlop);
                    grid.Children.Add(img);
                }
            }
        }
        private void PályaKirajzolás()
        {
            int skála = Settings.Skála;
            int szélesség = skála * pálya.Tábla.GetLength(0);
            int magasság = skála * pálya.Tábla.GetLength(1);

            játékablak.ResizeMode = ResizeMode.CanMinimize;
            játékablak.Height = szélesség + 38;
            játékablak.Width = magasság + 16;

            grid = new Grid();
            for (int i = 0; i < pálya.Tábla.GetLength(0); i++)
                grid.RowDefinitions.Add(new RowDefinition());
            for (int j = 0; j < pálya.Tábla.GetLength(1); j++)
                grid.ColumnDefinitions.Add(new ColumnDefinition());
            grid.Height = szélesség;
            grid.Width = magasság;
            grid.HorizontalAlignment = HorizontalAlignment.Center;
            grid.VerticalAlignment = VerticalAlignment.Center;

            játékablak.Content = grid;
        }
        private void PályaFrissítés()
        {
            for (int sor = 0; sor < pálya.Tábla.GetLength(0); sor++)
            {
                for (int oszlop = 0; oszlop < pálya.Tábla.GetLength(1); oszlop++)
                {
                    if (pálya.Tábla[sor, oszlop] == 0) //út
                    {
                        Grid.SetRow(utak[utak_index], pálya.Tábla.GetLength(0) - 1 - sor);
                        Grid.SetColumn(utak[utak_index++], oszlop);
                    }
                    if (pálya.Tábla[sor, oszlop] == 1) //kő
                    {
                        Grid.SetRow(kövek[kövek_index], pálya.Tábla.GetLength(0) - 1 - sor);
                        Grid.SetColumn(kövek[kövek_index++], oszlop);
                    }
                    if (pálya.Tábla[sor, oszlop] == 2) //ember
                    {
                        Grid.SetRow(játékosKép, pálya.Tábla.GetLength(0) - 1 - sor);
                        Grid.SetColumn(játékosKép, oszlop);
                    }
                    if (pálya.Tábla[sor, oszlop] == 3) //tűz
                    {
                        Grid.SetRow(tüzek[tüzek_index], pálya.Tábla.GetLength(0) - 1 - sor);
                        Grid.SetColumn(tüzek[tüzek_index++], oszlop);
                    }
                }
            }
            tüzek_index = 0;
            kövek_index = 0;
            utak_index = 0;
        }

        #region Háttérzene

        /// <summary>
        /// MediaPlayer a háttérzene lejátszásához
        /// </summary>
        MediaPlayer mediaPlayer = new MediaPlayer();
        /// <summary>
        /// Megnyitja a map.mp3 fájlt, feliratkoztatja a HáttérZeneEnded metódust a zene végét jelző
        /// eseményre, és ha be van állítva, akkor elindítja a háttérzenét.
        /// </summary>
        private void HáttérZene()
        {
            mediaPlayer.Open(new Uri(@"music/map.mp3", UriKind.Relative));      //megnyitja a map.mp3 fájlt
            mediaPlayer.MediaEnded += HáttérZeneEnded;                          //felirakozik a HáttérZeneEnded metódus a MediaEnded eseményre
            if (Settings.Háttérzene)                                            //ha kell háttérzene
                mediaPlayer.Play();                                             //akkor azt is elindítja
        }
        /// <summary>
        /// Ha a zene végére érünk, ez a metódus indítja újra az elejétől a lejátszást.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void HáttérZeneEnded(object sender, EventArgs e)
        {
            mediaPlayer.Position = TimeSpan.Zero; //ehhez elegendő csak a pozíciót nullára állítani
        }

        #endregion Háttérzene

        #region Események

        /// <summary>
        /// Kirajzolja a pályát, megjeleníti a kezdő MessageBox-ot, és elindítja a Timert, valamint
        /// meghívja a HáttérZene() metódust
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Window_Loaded_1(object sender, RoutedEventArgs e)
        {
            PályaKirajzolás();              //beállítja az ablakot és a gridet, majd felrakja a gridet
            KépeketListábaBerak();          //feltölti a képek listáit
            PályaFrissítés();               //betölti a gridbe a képeket a listákból

            MessageBox.Show("Nyomj Entert a kezdéshez!");
            this.Focus();                   //Ha esetleg nem lenne focusolva, akkor erőlteti a focust az ablakra

            //timer elindítása
            timer.Tick += timer_Tick;       //timer_Tick feliratkoztatása a timer.Tick eseményre
            timer.Interval = new TimeSpan(0, 0, 0, 0, Settings.TüzekSebessége_ms); //tűzsebesség beállítása a beállítások alapján
            timer.Start();                  //timer elindítása
            stopper.Start();                //stopper elindítása

            HáttérZene();                   //háttérzene elindítása
        }
        /// <summary>
        /// Megállítja a háttérzene lejátszását, a tüzek mozgását irányító timert,
        /// a pályaidőt mérő stoppert, és a beállításokban hamisra állítja a Paused értéket
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Window_Closing_1(object sender, System.ComponentModel.CancelEventArgs e)
        {
            mediaPlayer.Stop();         //Háttérzene megállítása
            timer.Stop();               //Tűzmozgató timer leállítása
            stopper.Stop();             //Pályaidőt mérő stopper leállítása
            Settings.Paused = false;    //Paused beállíás hamisra állítása
        }
        private void Window_KeyDown(object sender, KeyEventArgs e)
        {
            if (!Settings.Paused)                   //ha nincs megállítva a játék, akkor lehet mozogni
            {
                if (e.Key == Key.Left)              //Left
                    JátékosMozog(Irány.Balra);
                else if (e.Key == Key.Right)        //Right
                    JátékosMozog(Irány.Jobbra);
                else if (e.Key == Key.Up)           //Up
                    JátékosMozog(Irány.Fel);
                else if (e.Key == Key.Down)         //Down
                    JátékosMozog(Irány.Le);
            }
            if (e.Key == Key.Escape)            //Escape gomb nyomására
                this.Close();                       //bezárja az ablakot
            else if (e.Key == Key.P)            //P = Pause gomb nyomására
                PauseMegnyomva();                   //meghívja a PauseMegnyomva() függvényt
            else if (e.Key == Key.M)            //M = Mute gomb megnyomására
            {
                if (Settings.Háttérzene)        //Ha volt háttérzene, akkor
                    mediaPlayer.Pause();            //megállítja
                else                            //ha nem, akkor
                    mediaPlayer.Play();           //elindítja
                Settings.Háttérzene = !Settings.Háttérzene;     //és a beállításokban negálja a beállítást
            }
            else if (e.Key == Key.L)            //L = Level-Up megnyomására
            { //tesztelési célokra használható "hack", csak saját használatra
                string user = System.Security.Principal.WindowsIdentity.GetCurrent().Name; //felveszi az adott felhasználó nevét és gépnevét
                if (user.Contains("OKARI") && user.Contains("Karcsi")) //ha ez az én számítógépem, akkor
                    EredményKiírás();           //megnyeri a játékot
            }
        }
        void timer_Tick(object sender, EventArgs e)
        {
            //Egy időegység elteltekor mozgatja az összes tüzet
            for (int i = 0; i < pálya.Tüzek.Count; i++) //minden tűzön végigmegy (pálya.Tüzek lista)
            {
                if (!pálya.Tüzek[i].Bekerítve)     //ha az adott tűz nincs bekerítve
                {
                    //kivesz egy random eleme az irányokat tartalmazó tömbből:
                    Irány randomIrány = (Irány)irányÉrtékek.GetValue(r.Next(irányÉrtékek.Length));
                    //meghatározza, hogy abba az irányba mi van:
                    int irányábamivan = IránybaMiVan(randomIrány, pálya.Tüzek[i].X, pálya.Tüzek[i].Y);

                    if (irányábamivan == 0) //ha irányába út van (= 0), akkor
                    {
                        if (randomIrány == Irány.Balra)         //ha balra megy, akkor balra lépteti az objektumot a táblán
                            ObjektumLépTáblán(pálya.Tüzek[i].X, pálya.Tüzek[i].Y - 1, pálya.Tüzek[i].X, pálya.Tüzek[i].Y, 3);
                        else if (randomIrány == Irány.Jobbra)   //ha jobbra megy, akkor jobbra lépteti az objektumot a táblán
                            ObjektumLépTáblán(pálya.Tüzek[i].X, pálya.Tüzek[i].Y + 1, pálya.Tüzek[i].X, pálya.Tüzek[i].Y, 3);
                        else if (randomIrány == Irány.Fel)      //ha felfelé megy, akkor felfelé lépteti az objektumot a táblán
                            ObjektumLépTáblán(pálya.Tüzek[i].X + 1, pálya.Tüzek[i].Y, pálya.Tüzek[i].X, pálya.Tüzek[i].Y, 3);
                        else                                    //ha lefelé megy, akkor lefelé lépteti az objektumot a táblán
                            ObjektumLépTáblán(pálya.Tüzek[i].X - 1, pálya.Tüzek[i].Y, pálya.Tüzek[i].X, pálya.Tüzek[i].Y, 3);

                        pálya.Tüzek[i].Mozog(randomIrány);      //és a tűz objektum adatait is frissíti
                    }
                    else if (irányábamivan == 2)    //ha irányába játékos van (= 2), akkor
                    {
                        játékos.ÉletbenVan = false; //megöli a játékost
                        EredményKiírás();           //és véget vet a játéknak (kiírja az eredményeket)
                    }
                }
            }
            PályaFrissítés(); //Minden mozgatás után újrarajzolja a pályát
        }

        #endregion

        #region Mozgató metódusok

        /// <summary>
        /// Mozgatja a játékost egy egységgel a paraméterként megadott irányba.
        /// </summary>
        /// <param name="irány">Irány enum tag: megadja, hogy merre mozogjon a játékos</param>
        private void JátékosMozog(Irány irány)
        {
            //pálya.Táblával szinkronizált mozgás
            #region Mozgás

            int irányábamivan = IránybaMiVan(irány, játékos.X, játékos.Y); //megadja, hogy a lépni kívánt irányba milyen objektum van

            if (irány == Irány.Balra)                       //ha balra akar mozogni a játékos, akkor
            {
                if (irányábamivan == 0)                     //ha irányában út van, akkor
                { //simán tud lépni, tehát
                    ObjektumLépTáblán(játékos.X, játékos.Y - 1, játékos.X, játékos.Y, 2);   //a játékost lépteti eggyel balra a táblán
                    játékos.Mozog(irány);                                                   //és a játékos objektum adatait is frissíti
                }
                else if (irányábamivan == 1)                //ha irányában kő van, akkor
                { //el kell tolnia a követ
                    if (KövetMozgat(irány, játékos.X, játékos.Y - 1)) //ha az adott irányba lehet mozgatni az adott helyen lévő követ, akkor
                    { //mozgatja a követ az adott helyre
                        ObjektumLépTáblán(játékos.X, játékos.Y - 1, játékos.X, játékos.Y, 2);   //a követ lépteti eggyel balra a táblán
                        játékos.Mozog(irány);                                                   //és a játékos objektum adatait is frissíti
                    }
                }
                else if (irányábamivan == 3)                //ha irányában tűz van, akkor
                { //a játékos meghal
                    játékos.ÉletbenVan = false;             //megöli a játékost
                    EredményKiírás();                       //és véget vet a játéknak, azaz kiírja az eredményeket
                }
            }
            else if (irány == Irány.Jobbra)                 //ha jobbra akar mozogni a játékos, akkor
            {
                if (irányábamivan == 0)                     //ha irányában út van, akkor
                { //simán tud lépni, tehát
                    ObjektumLépTáblán(játékos.X, játékos.Y + 1, játékos.X, játékos.Y, 2);   //a játékost lépteti eggyel balra a táblán
                    játékos.Mozog(irány);                                                   //és a játékos objektum adatait is frissíti
                }
                else if (irányábamivan == 1)                //ha irányában kő van, akkor
                { //el kell tolnia a követ
                    if (KövetMozgat(irány, játékos.X, játékos.Y + 1)) //ha az adott irányba lehet mozgatni az adott helyen lévő követ, akkor
                    { //mozgatja a követ az adott helyre
                        ObjektumLépTáblán(játékos.X, játékos.Y + 1, játékos.X, játékos.Y, 2);   //a követ lépteti eggyel balra a táblán
                        játékos.Mozog(irány);                                                   //és a játékos objektum adatait is frissíti
                    }
                }
                else if (irányábamivan == 3)                //ha irányában tűz van, akkor
                { //a játékos meghal
                    játékos.ÉletbenVan = false;             //megöli a játékost
                    EredményKiírás();                       //és véget vet a játéknak, azaz kiírja az eredményeket
                }
            }
            else if (irány == Irány.Fel)
            {
                if (irányábamivan == 0) //ha útra lépne
                {
                    ObjektumLépTáblán(játékos.X + 1, játékos.Y, játékos.X, játékos.Y, 2);
                    játékos.Mozog(irány); //és a játékos objektum adatait is frissítjük
                }
                else if (irányábamivan == 1) //ha kőre lépne
                {
                    if (KövetMozgat(irány, játékos.X + 1, játékos.Y)) //ha az adott irányba lehet mozgatni a követ
                    { //akkor mozgatja a követ az adott helyre
                        ObjektumLépTáblán(játékos.X + 1, játékos.Y, játékos.X, játékos.Y, 2);
                        játékos.Mozog(irány); //és a játékos objektum adatait is frissítjük
                    }
                }
                else if (irányábamivan == 3) //ha tűzre lépne
                {
                    játékos.ÉletbenVan = false;
                    EredményKiírás();
                }
            }
            else //ha lefelé akar mozogni a játékos
            {
                if (irányábamivan == 0) //ha útra lépne
                {
                    ObjektumLépTáblán(játékos.X - 1, játékos.Y, játékos.X, játékos.Y, 2);
                    játékos.Mozog(irány); //és a játékos objektum adatait is frissítjük
                }
                else if (irányábamivan == 1) //ha kőre lépne
                {
                    if (KövetMozgat(irány, játékos.X - 1, játékos.Y)) //ha az adott irányba lehet mozgatni a követ
                    { //akkor mozgatja a követ az adott helyre
                        ObjektumLépTáblán(játékos.X - 1, játékos.Y, játékos.X, játékos.Y, 2);
                        játékos.Mozog(irány); //és a játékos objektum adatait is frissítjük
                    }
                }
                else if (irányábamivan == 3) //ha tűzre lépne
                {
                    játékos.ÉletbenVan = false;
                    EredményKiírás();
                }
            }

            #endregion
            PályaFrissítés();
            TűzBekerítésekFrissítése();
            JátékVégeVizsgálat();
        }
        /// <summary>
        /// Ha a kő mozgatható az adott helyre, akkor mozgatja és igazzal tér vissza.
        /// </summary>
        /// <param name="irány">Az irány, amerre mozgatni akarjuk</param>
        /// <param name="kőXPoz">A mozqatni kívánt kő X pozíciója</param>
        /// <param name="kőYPoz">A mozgatni kívánt kő Y pozíciója</param>
        /// <returns></returns>
        private bool KövetMozgat(Irány irány, int kőXPoz, int kőYPoz)
        {
            if (IránybaMiVan(irány, kőXPoz, kőYPoz) > 0) //ha ahova mozognánk, ott nem út van, akkor nem mozgat
                return false;
            //itt már tudjuk, hogy az irányba mozgatható a kő
            if (irány == Irány.Balra)
                pálya.Tábla[kőXPoz, kőYPoz - 1] = 1; //a követ eggyel balra mozgatjuk
            if (irány == Irány.Jobbra)
                pálya.Tábla[kőXPoz, kőYPoz + 1] = 1; //a követ eggyel jobbra mozgatjuk
            if (irány == Irány.Fel)
                pálya.Tábla[kőXPoz + 1, kőYPoz] = 1; //a követ eggyel fel mozgatjuk
            if (irány == Irány.Le)
                pálya.Tábla[kőXPoz - 1, kőYPoz] = 1; //a követ eggyel le mozgatjuk
            return true;
        }
        /// <summary>
        /// Az adott objektumot lépteti a táblán.
        /// </summary>
        /// <param name="hovaX"></param>
        /// <param name="hovaY"></param>
        /// <param name="miLép">Milyen objektumot léptetsz? 1 = kő, 2 = ember, 3 = tűz</param>
        /// <param name="honnanX"></param>
        /// <param name="honnanY"></param>
        private void ObjektumLépTáblán(int hovaX, int hovaY, int honnanX, int honnanY, byte miLép)
        {
            pálya.Tábla[hovaX, hovaY] = miLép; //az objektum helyére lép
            pálya.Tábla[honnanX, honnanY] = 0; //az objektum helyére teszünk egy utat
        }

        #endregion

        #region Ellenőrző metódusok

        /// <summary>
        /// Ha az adott irányba lépve nem mennénk ki a pályáról, akkor igazat ad.
        /// </summary>
        /// <param name="irány"></param>
        /// <param name="xPoz"></param>
        /// <param name="yPoz"></param>
        /// <returns></returns>
        private bool IránybaLehetMenni(Irány irány, int xPoz, int yPoz)
        {
            if (irány == Irány.Balra)
                return yPoz > 0; //ha nem vagyunk a bal szélén
            else if (irány == Irány.Jobbra)
                return yPoz < pálya.Tábla.GetLength(1) - 1; //ha nem vagyunk a jobb szélén
            else if (irány == Irány.Fel)
                return xPoz < pálya.Tábla.GetLength(0) - 1; //ha nem vagyunk a tetején
            else if (irány == Irány.Le)
                return xPoz > 0; //ha nem vagyunk az alján

            return false; //szintaktikai jelentőségű sor
        }
        /// <summary>
        /// Megadja, hogy az adott irányban mi van, feltéve, hogy oda lehet lépni.
        /// Ha nem lehet oda lépni, akkor 100-al tér vissza.
        /// </summary>
        /// <param name="irány">A vizsgálandó irány (Irány enum) </param>
        /// <param name="xPoz">Az objektum pozíciójának X koordinátája</param>
        /// <param name="yPoz">Az objektum pozíciójának Y koordinátája</param>
        /// <returns></returns>
        private int IránybaMiVan(Irány irány, int xPoz, int yPoz)
        {
            //Azért 100-al térünk vissza, mert a KövetMozgat metódusnál így egyszerű azt vizsgálni, hogy
            //az adott helyre mozgathatjuk -e a követ.
            if (!IránybaLehetMenni(irány, xPoz, yPoz))
                return 100;
            //De ha lehet abba az irányba menni, akkor megyünk:
            if (irány == Irány.Balra)
                return pálya.Tábla[xPoz, yPoz - 1];
            if (irány == Irány.Jobbra)
                return pálya.Tábla[xPoz, yPoz + 1];
            if (irány == Irány.Fel)
                return pálya.Tábla[xPoz + 1, yPoz];
            if (irány == Irány.Le)
                return pálya.Tábla[xPoz - 1, yPoz];

            return 100; //szintaktikai szerepe van
        }

        private void TűzBekerítésekFrissítése()
        {
            for (int i = 0; i < pálya.Tüzek.Count; i++)
            {
                if (AdottTűzBeVanKerítve(pálya.Tüzek[i]))
                    pálya.Tüzek[i].Bekerítve = true;
                else //ha esetleg időközben egy tüzet "kikerítünk", akkor azt is át kell írni
                    pálya.Tüzek[i].Bekerítve = false;
            }
        }
        private bool AdottTűzBeVanKerítve(Tuz t)
        {
            //Ha minden irányban épp nem az ember van, de nem út (azaz kő vagy tűz)
            if (IránybaMiVan(Irány.Balra, t.X, t.Y) != 2 && IránybaMiVan(Irány.Balra, t.X, t.Y) > 0 &&
                IránybaMiVan(Irány.Jobbra, t.X, t.Y) != 2 && IránybaMiVan(Irány.Jobbra, t.X, t.Y) > 0 &&
                IránybaMiVan(Irány.Fel, t.X, t.Y) != 2 && IránybaMiVan(Irány.Fel, t.X, t.Y) > 0 &&
                IránybaMiVan(Irány.Le, t.X, t.Y) != 2 && IránybaMiVan(Irány.Le, t.X, t.Y) > 0)
                return true;
            else
                return false;
        }
        private void JátékVégeVizsgálat()
        {
            if (pálya.ÖsszesTűzBekerítve)
                EredményKiírás();
        }
        #endregion

        #region Egyéb metódusok (EredményKiírás, IdőKonverter, Pause)

        private void EredményKiírás()
        {
            timer.Stop();
            stopper.Stop();
            string idő = IdőKonverter(stopper.Elapsed);

            if (játékablak.Játékos.ÉletbenVan)
            {
                if (Settings.HanyadikPálya < 50)
                {
                    Settings.TüzekSzáma++; //játék végén növeljük a tüzek számát
                    Settings.KövekSzáma += Settings.PályánkéntHányKővelNő;
                }
                if (Settings.HanyadikPálya < 30)
                {
                    Settings.SorokSzáma++;
                    Settings.OszlopokSzáma++;
                }
                if (Settings.Skála > 15) //egy bizonyos határig csökken csak a skála
                    Settings.Skála--;

                if (Settings.HanyadikPálya > 50) //ha 50. pályán túl van, akkor pályánként már 2 ms-sel gyorsulnak a tüzek
                    Settings.TüzekSebessége_ms -= 2;
                else
                    Settings.TüzekSebessége_ms--;

                Settings.HanyadikPálya++;
                MessageBox.Show(String.Format("Gratulálok, nyertél! :)\n\nAz időd:  {0}", idő), "Játék vége");
            }
            else //meghalt, "reset" következik
            {
                Settings.TüzekSzáma = Settings.Original_TüzekSzáma;
                Settings.KövekSzáma = Settings.Original_KövekSzáma;
                Settings.SorokSzáma = Settings.Original_SorokSzáma;
                Settings.OszlopokSzáma = Settings.Original_OszlopokSzáma;
                Settings.Skála = Settings.Original_Skála;
                Settings.HanyadikPálya = 1;
                MessageBox.Show(String.Format("Vesztettél!\n\nAz időd:  {0}\n\nMost kezdheted elölről :(", idő), "Játék vége");
            }

            DialogResult = true;
        }
        private string IdőKonverter(TimeSpan elapsed)
        {
            int p = elapsed.Minutes;
            int mp = elapsed.Seconds;
            int tizedmp = elapsed.Milliseconds / 100;

            if (elapsed.Hours > 0)
                return "több, mint 1 óra!";
            else
                return String.Format("{0:00}:{1:00}.{2:00}", p, mp, tizedmp);
        }
        private void PauseMegnyomva()
        {
            if (Settings.Paused) //ha meg volt megállítva
            {
                grid = gridsave;
                this.Content = grid;
                timer.Start();
            }
            else //ha nem volt megállítva
            {
                timer.Stop();
                gridsave = grid; //Előző grid elmentése
                Label pauseLabel = new Label();
                PausetBeállít(pauseLabel);
                grid = new Grid();
                grid.Children.Add(pauseLabel);
                grid.Background = Brushes.Black;
                this.Content = grid;
            }

            Settings.Paused = !Settings.Paused; //kapcsoló átkapcsolása
        }
        private void PausetBeállít(Label pauseLabel)
        {
            pauseLabel.Content = "Megállítva";
            pauseLabel.FontSize = 50;
            pauseLabel.FontFamily = new FontFamily("Chiller");
            pauseLabel.Foreground = Brushes.Red;
            pauseLabel.BorderBrush = Brushes.Black;
            pauseLabel.Background = Brushes.Black;
            pauseLabel.HorizontalAlignment = HorizontalAlignment.Center;
            pauseLabel.VerticalAlignment = VerticalAlignment.Center;
            pauseLabel.Margin = new Thickness(0, 0, 0, this.Height * 0.2); //picit a közepétől fentebb
        }

        #endregion
    }
}